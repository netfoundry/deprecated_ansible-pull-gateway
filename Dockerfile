FROM alpine:3.10
RUN apk update
RUN apk upgrade
RUN apk add gpgme \
            bash \
            py-pip \
            gcc \
            python2-dev \
            linux-headers \
            musl-dev
RUN apk --update add git openssh && \
    rm -rf /var/lib/apt/lists/* && \
    rm /var/cache/apk/*
RUN pip install pretty-bad-protocol \
                pyyaml
CMD bash
#!/usr/bin/env python
#
# params:
#   path to YAML file ./signers.yml
#
# Error if pubkey expiry is too far in the future or not far enough
#

from pretty_bad_protocol import gnupg
import yaml
import datetime
import sys
import os


NOW = datetime.datetime.utcnow()
MIN_DAYS=180
MAX_DAYS=365*2
"""
MIN_EXPIRY = NOW + datetime.timedelta(days=MIN_DAYS)
MAX_EPIRY = NOW + datetime.timedelta(days=MAX_DAYS)
MIN_STR = MIN_EXPIRY.strftime("%m/%d/%Y")
MAX_STR = MAX_EPIRY.strftime("%m/%d/%Y")
"""

SIGNERS = yaml.full_load(open(sys.argv[1],'r'))['signers']
KEYS = [s['gpgpubkey'] for s in SIGNERS]

if os.environ['GNUPGHOME']:
    GPG = gnupg.GPG(homedir=os.environ['GNUPGHOME'])
else:
    GPG = gnupg.GPG()

for key in KEYS:
    GPG.import_keys(key)

for key in GPG.list_keys():
    uid = key["uids"]
    keyid = key["keyid"]
    if 'expires' in key and key['expires']:
        expires = key['expires']
        fl_expires = datetime.datetime.fromtimestamp( float(expires) )
        str_expire = datetime.datetime.fromtimestamp( float(expires) ).strftime('%Y-%m-%d %H:%M:%S')
        days_to_expire = int((fl_expires - NOW).days)
        if days_to_expire < MIN_DAYS:
            raise Exception(" ** Key ( {} ) {} expiry is not far enough in the future ({}) **\n".format(keyid, uid, str_expire))
        elif days_to_expire > MAX_DAYS:
            raise Exception(" ** Key ( {} ) {} expires too far in the future ({}) **\n".format(keyid, uid, days_to_expire, str_expire))
        else:
            print(" Key ( {} ) {} expires in {} days ({}) \n".format(keyid, uid, days_to_expire, str_expire))
    else:
        raise Exception("No expiration date ( {} ) {} \n".format(keyid, uid))
    
